EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:tp5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3850 2900 0    60   Input ~ 0
GND
Text HLabel 3850 3250 0    60   Input ~ 0
GPIO1
Text HLabel 3900 3600 0    60   Input ~ 0
GPIO0
Text HLabel 3750 4000 0    60   Input ~ 0
RX
Text HLabel 10000 2800 2    60   Input ~ 0
VCC
Text HLabel 10000 3200 2    60   Input ~ 0
RST
Text HLabel 9950 3550 2    60   Input ~ 0
CHPD
Text HLabel 9950 4000 2    60   Input ~ 0
TX
$EndSCHEMATC
