EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:ESP8266
LIBS:bme280
LIBS:switches
LIBS:ky-040_encoder
LIBS:tp5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7500 1900 1300 450 
U 576552B0
F0 "Fuente de Alimentación" 60
F1 "fuente.sch" 60
F2 "Vin" I L 7500 2050 60 
$EndSheet
$Sheet
S 5800 2800 1350 850 
U 5765531B
F0 "CPU" 60
F1 "cpu.sch" 60
F2 "SCL" I R 7150 3050 60 
F3 "SDA" I R 7150 3400 60 
F4 "GPIO1_[FUNC2:_U0_TXD]" I L 5800 3300 60 
F5 "GPIO2_[FUNC2:_U0_RXD]" I L 5800 3450 60 
F6 "GPIO0_[FUNC0:_GPIO3[0]]" I L 5800 3600 60 
F7 "GPIO5_[FUNC0:_GPIO3[5]]" I L 5800 2850 60 
F8 "GPIO6_[FUNC0:_GPIO3[6]]" I L 5800 2950 60 
F9 "GPIO7_[FUNC0:_GPIO3[7]]" I L 5800 3150 60 
$EndSheet
$Comp
L BARREL_JACK CON1
U 1 1 5765553F
P 6900 2150
F 0 "CON1" H 6750 2350 50  0000 C CNN
F 1 "Entrada Alimentación" H 6800 2000 50  0000 C CNN
F 2 "Connectors:BARREL_JACK" H 6900 2150 50  0001 C CNN
F 3 "" H 6900 2150 50  0000 C CNN
	1    6900 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 57656C1E
P 7300 2400
F 0 "#PWR01" H 7300 2150 50  0001 C CNN
F 1 "GND" H 7300 2250 50  0000 C CNN
F 2 "" H 7300 2400 50  0000 C CNN
F 3 "" H 7300 2400 50  0000 C CNN
	1    7300 2400
	1    0    0    -1  
$EndComp
$Comp
L ESP-01v090 U4
U 1 1 59CF9CB0
P 6350 4450
F 0 "U4" H 6350 4350 50  0000 C CNN
F 1 "ESP-01v090" H 6350 4550 50  0000 C CNN
F 2 "ESP8266:ESP-01" H 6350 4450 50  0001 C CNN
F 3 "" H 6350 4450 50  0001 C CNN
	1    6350 4450
	1    0    0    -1  
$EndComp
$Comp
L BME280 U5
U 1 1 59CFC820
P 8550 3400
F 0 "U5" H 8550 3550 60  0000 C CNN
F 1 "BME280" H 8550 3400 60  0000 C CNN
F 2 "footprints:BME280" H 8550 3400 60  0001 C CNN
F 3 "" H 8550 3400 60  0001 C CNN
	1    8550 3400
	1    0    0    -1  
$EndComp
NoConn ~ 7300 4500
$Comp
L +3.3V #PWR02
U 1 1 59D591EB
P 8250 4450
F 0 "#PWR02" H 8250 4300 50  0001 C CNN
F 1 "+3.3V" H 8250 4590 50  0000 C CNN
F 2 "" H 8250 4450 50  0001 C CNN
F 3 "" H 8250 4450 50  0001 C CNN
	1    8250 4450
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 59D5AC7E
P 4700 4150
F 0 "#PWR03" H 4700 4000 50  0001 C CNN
F 1 "+3.3V" H 4700 4290 50  0000 C CNN
F 2 "" H 4700 4150 50  0001 C CNN
F 3 "" H 4700 4150 50  0001 C CNN
	1    4700 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 59D5BF68
P 7750 4350
F 0 "#PWR04" H 7750 4100 50  0001 C CNN
F 1 "GND" H 7750 4200 50  0000 C CNN
F 2 "" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0001 C CNN
	1    7750 4350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 59D8EC33
P 8400 4500
F 0 "#PWR05" H 8400 4250 50  0001 C CNN
F 1 "GND" H 8400 4350 50  0000 C CNN
F 2 "" H 8400 4500 50  0001 C CNN
F 3 "" H 8400 4500 50  0001 C CNN
	1    8400 4500
	1    0    0    -1  
$EndComp
NoConn ~ 8850 3700
NoConn ~ 8550 3700
NoConn ~ 8100 3950
NoConn ~ 7300 4400
$Comp
L R R3
U 1 1 59D969C0
P 4850 4500
F 0 "R3" V 4930 4500 50  0000 C CNN
F 1 "10K" V 4850 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4780 4500 50  0001 C CNN
F 3 "" H 4850 4500 50  0001 C CNN
	1    4850 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	7500 2050 7200 2050
Wire Wire Line
	7200 2150 7300 2150
Wire Wire Line
	7300 2150 7300 2400
Wire Wire Line
	7300 2250 7200 2250
Connection ~ 7300 2250
Wire Wire Line
	8050 3300 7550 3300
Wire Wire Line
	4700 4600 5400 4600
Wire Wire Line
	4700 4150 4700 4600
Wire Wire Line
	7300 4300 7750 4300
Wire Wire Line
	7750 4300 7750 4350
Wire Wire Line
	4700 4400 5400 4400
Connection ~ 4700 4400
Wire Wire Line
	4850 4300 5400 4300
Wire Wire Line
	7300 4600 7300 5100
Wire Wire Line
	7300 5100 4550 5100
Connection ~ 4700 4500
Wire Wire Line
	9000 3700 9000 4400
Wire Wire Line
	8250 3950 8250 4450
Wire Wire Line
	8400 3700 8400 4500
Wire Wire Line
	5000 4500 5400 4500
Connection ~ 5100 4500
Wire Wire Line
	9000 4400 8250 4400
Connection ~ 8250 4400
Wire Wire Line
	8700 3700 8700 4250
Wire Wire Line
	8700 4250 8400 4250
Connection ~ 8400 4250
Wire Wire Line
	8050 3150 7550 3150
Wire Wire Line
	7550 3150 7550 3050
Wire Wire Line
	7550 3050 7150 3050
Wire Wire Line
	7150 3400 7550 3400
Wire Wire Line
	7550 3400 7550 3300
$Sheet
S 3000 2600 1150 750 
U 59F61227
F0 "Hall Effect Sensor" 60
F1 "Hall_Effect_Sensor.sch" 60
F2 "P" I R 4150 3150 60 
$EndSheet
$Sheet
S 4300 1950 1200 850 
U 59FDBD58
F0 "Encoder_KY-040" 60
F1 "Encoder_KY-040.sch" 60
F2 "CLK" I R 5500 2200 60 
F3 "DT" I R 5500 2550 60 
$EndSheet
Wire Wire Line
	5800 3450 4850 3450
Wire Wire Line
	4850 3450 4850 4300
Wire Wire Line
	5100 4500 5100 3600
Wire Wire Line
	5100 3600 5800 3600
Wire Wire Line
	4550 5100 4550 3300
Wire Wire Line
	4550 3300 5800 3300
Wire Wire Line
	5800 2850 5700 2850
Wire Wire Line
	5600 2950 5800 2950
Wire Wire Line
	4150 3150 5800 3150
Wire Wire Line
	5500 2550 5600 2550
Wire Wire Line
	5600 2550 5600 2950
Wire Wire Line
	5700 2850 5700 2200
Wire Wire Line
	5700 2200 5500 2200
$EndSCHEMATC
