EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:ESP8266
LIBS:bme280
LIBS:switches
LIBS:ky-040_encoder
LIBS:tp5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KY-040_Encoder U8
U 1 1 59FDBD7F
P 4350 4100
F 0 "U8" H 4500 4400 60  0000 C CNN
F 1 "KY-040_Encoder" H 4650 4250 60  0000 C CNN
F 2 "footprints:KY-040_Encoder" H 4350 4100 60  0001 C CNN
F 3 "" H 4350 4100 60  0001 C CNN
	1    4350 4100
	1    0    0    -1  
$EndComp
NoConn ~ 5450 3800
$Comp
L GND #PWR048
U 1 1 59FDBE30
P 5700 4100
F 0 "#PWR048" H 5700 3850 50  0001 C CNN
F 1 "GND" H 5700 3950 50  0000 C CNN
F 2 "" H 5700 4100 50  0001 C CNN
F 3 "" H 5700 4100 50  0001 C CNN
	1    5700 4100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR049
U 1 1 59FDBE46
P 6100 3100
F 0 "#PWR049" H 6100 2950 50  0001 C CNN
F 1 "+5V" H 6100 3240 50  0000 C CNN
F 2 "" H 6100 3100 50  0001 C CNN
F 3 "" H 6100 3100 50  0001 C CNN
	1    6100 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3950 5900 3950
Wire Wire Line
	5450 4100 5700 4100
Text HLabel 6850 3500 2    60   Input ~ 0
CLK
Text HLabel 6850 3650 2    60   Input ~ 0
DT
Wire Wire Line
	5450 3500 6850 3500
Wire Wire Line
	5450 3650 6850 3650
$Comp
L R R11
U 1 1 59FDBE90
P 5700 3350
F 0 "R11" V 5780 3350 50  0000 C CNN
F 1 "R" V 5700 3350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5630 3350 50  0001 C CNN
F 3 "" H 5700 3350 50  0001 C CNN
	1    5700 3350
	1    0    0    -1  
$EndComp
Connection ~ 5700 3500
Wire Wire Line
	5700 3200 6100 3200
Wire Wire Line
	6100 3100 6100 3300
$Comp
L R R12
U 1 1 59FDBF55
P 6100 3450
F 0 "R12" V 6180 3450 50  0000 C CNN
F 1 "R" V 6100 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6030 3450 50  0001 C CNN
F 3 "" H 6100 3450 50  0001 C CNN
	1    6100 3450
	1    0    0    -1  
$EndComp
Connection ~ 6100 3200
Wire Wire Line
	6100 3600 6100 3650
Connection ~ 6100 3650
Wire Wire Line
	5900 3950 5900 3200
Connection ~ 5900 3200
$EndSCHEMATC
