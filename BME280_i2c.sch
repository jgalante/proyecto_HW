EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:tp5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4150 2200 3000 2600
U 59C2FA70
F0 "Sheet59C2FA6F" 60
F1 "BME280.sch" 60
$EndSheet
Text HLabel 8800 3000 2    60   Input ~ 0
SDA
Text HLabel 8800 3550 2    60   Input ~ 0
SCL
Text HLabel 3000 4000 0    60   Input ~ 0
I2C
Text HLabel 3550 1300 3    60   Input ~ 0
Vdd
Text HLabel 4050 1300 3    60   Input ~ 0
Vddio
$EndSCHEMATC
